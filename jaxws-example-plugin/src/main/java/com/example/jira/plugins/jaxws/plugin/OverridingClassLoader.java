package com.example.jira.plugins.jaxws.plugin;

import java.io.InputStream;

/**
 * A horrible hack. Something in JIRA or a dependency we touch overrides the default XMLInputFactory service to an
 * implementation in the 'org.codehaus.woodstox:wstx-asl library'. This is bundled in JIRA, but not exported to OSGi and
 * thus unavailable to a plugins-2 plugin. We can't bundle wstx-asl ourselves because it brings in some other clashing
 * classes that cause other problems (something in stax-api, if I recall correctly?). Therefore, whenever we
 * invoke JAX-WS, we'll inject hacky ClassLoader that sets the XMLInputFactory back to the default Sun JDK one.
 */
public class OverridingClassLoader extends ClassLoader
{
    private final String XML_OUTPUT_FACTORY_SERVICE = "META-INF/services/javax.xml.stream.XMLOutputFactory";
    private final String XML_INPUT_FACTORY_SERVICE = "META-INF/services/javax.xml.stream.XMLOutputFactory";

    private final ClassLoader parent;

    public OverridingClassLoader(ClassLoader parent)
    {
        this.parent = parent;
    }

    /**
     * This gets called by {@link javax.xml.stream.FactoryFinder}
     */
    @Override
    public InputStream getResourceAsStream(String name)
    {
        if (XML_OUTPUT_FACTORY_SERVICE.equals(name) ||
            XML_INPUT_FACTORY_SERVICE.equals(name))
        {
            // Perform normal ClassLoader handling for these services - look in the plugin's META-INF/services namespace.
            return super.getResourceAsStream(name);
        }

        // Delegate to the plugin's OSGi ClassLoader.
        return parent.getResourceAsStream(name);
    }
}
